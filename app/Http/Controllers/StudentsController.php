<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function index()
    {
      return Student::all();
    }

    public function store(Request $request)
    {
        $inputs = $request->input();
        $e = Student::create($inputs);
        return response()->json([
            'data'=>$e,
            'message'=>"Student added.",
        ]); 
    }

    public function show($id)
    {
        $e = Student::find($id);
        if(isset($e)){
                return response()->json([
                    'data'=>$e,
                    'message'=>"Student found.",
                ]);
            }else{
                return response()->json([
                    'error'=>true,
                    'message'=>"This student doesn't found.",
                ]);
            }
    }
    
    public function update(Request $request, $id)
    {
        $e = Student::find($id);
        if(isset($e)){
            $e->name = $request->name;
            $e->last_name = $request->last_name;
            $e->photo = $request->photo;
            if($e->save()){
                return response()->json([
                    'data'=>$e,
                    'message'=>"Student updated.",
                ]);
            }else{
                return response()->json([
                    'error'=>true,
                    'message'=>"This student doesn't updated.",
                ]);
            }
        }else{
            return response()->json([
                'error'=>true,
                'mensaje'=>"This Student doesn't exist.",
            ]);
        }
    }

    public function destroy($id)
    {
        $e = Student::find($id);
        if(isset($e)){
               $response = Student::destroy($id);
               if($response){
                return response()->json([
                    'data'=>$e,
                    'message'=>"Student deleted.",
                ]);
            }else{
                return response()->json([
                    'data'=>$e,
                    'message'=>"Student doesn't exist.",
                ]);
            }
        }else{
                return response()->json([
                    'error'=>true,
                    'mensaje'=>"This Student doesn't exist.",
                ]);
            }
         } 
    }




